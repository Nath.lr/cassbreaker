

#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080
#define FPS 60
#define tabMax 18



/*/////////////////////////////////////////Variables\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
/*/////////////////////////////////////////Variables\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


/*-----------------------------------------Element de jeux-----------------------------------*/

            //Variables Balle

    int ballx;
    int bally;
    int rayonBall;
    int vitesseX;
    int vitesseY;

            //Variables trainé Balle

    int ball2x;
    int ball2y;
    int rayonBall2;
    int ball3x;
    int ball3y;
    int rayonBall3;

            //Variables Barre

    int xBarre;
    int yBarre;
    int longBarre;
    int hauteurBarre;
    int deplTouche;
    int couleurBarre;

            //Variables Barre2

    int xBarre2;
    int yBarre2;
    int longBarre2;
    int hauteurBarre2;
    int deplTouche2;

            //Variables Briques

    int xBrique;
    int yBrique;
    int xEcart;
    int yEcart;
    int longBrique;
    int hauteurBrique;
    int espaceBrique;
    int nbcolonne;
    int nbligne;
    int ytabmulti;
    int xtabmulti;

            //Variables Briques

    int largFenetre;

/*-----------------------------------------Gameplay------------------------------------------*/

            //Vie Briques

    int viebrique[9][5];
    int vieMulti[9][5];
    int i;
    int j;
    int nbBrique;
    int briqueTouche;
    int nbBriqueMulti;
    int briqueToucheMulti;

            //Gameplay

    int nbvie;
    int nbvieJ2;
    int tentative;
    int tentativeJ2;
    float diminutionBarre;
    int PauseGame;
    int reinittab;
    int timeinittab;
    int accelPalier;
    int palierVitesse;
    int movex;
    int movey;
    int moveYorN;
    int xmoveYorN;
    int toucheQ;
    int toucheD;
    int toucheLEFT;
    int toucheRIGHT;

    int keyState[SDL_NUM_SCANCODES] = {0};

/*-----------------------------------------Menu----------------------------------------------*/

    int start;
    int histoire;
    int arcade;
    int solo;
    int multi;
    int coop;
    int versus;


/*-----------------------------------------Affichage-----------------------------------------*/

            //Accueil
        
    int xrectLogo;
    int yrectLogo;
    int longLogo;
    int hautLogo;

    int yfond;
    int xdebutjeu;
    int ydebutjeu;


            //Menu

    int longmenu;
    int xrectMenu;
    int menuHistoire;
    int menuXarcade;
    int menuSolo;
    int menuMulti;
    int menuCoop;
    int menuVersus;
    int menuY;
    int menuY2;
    int yavamenu;
    int ybonjour;

            //Gameover
        
    int xrectGameOver;
    int yrectGameOver;
    int longGameOver;
    int hautGameOver;

    int xDefaite;
    int yAvaDefaite;

    int xfin1;
    int xfin2;
    int xfin3;
    int xfin4;
    int xfin5;
    int xfinj1;
    int xfinj2;

            //Compteur

    int compteurBrique1;
    int compteurBrique2;
    int compteurBrique3;
    int compteurBrique4;
    int compteurBrique5;

    int compteurPallier1;
    int compteurPallier2;
    int compteurPallier3;
    int compteurPallier4;
    int compteurPallier5;

    int CompteurVieJ1;
    int CompteurVieJ2;

            //Victoire
        
    int xrectVictoire;
    int yrectVictoire;
    int longVictoire;
    int hautVictoire;

    int xVictoire;
    int yAvavictoire;
    int vitVictoire;
    int avavitVictoire;


            //Mission

    int xkitty;
    int ykitty;
    int vitesseAssetX;
    int vitesseAssetY;

            //Asset Jeux
    int fond;
    int xAvavie;
    int yAvavie;
    int xAvavie2;
    int yAvavie2;
    int vitesseAvavieY;
    int xAvavitesse;
    int yAvavitesse;
    int xAvavitesse2;
    int yAvavitesse2;
    int xAvavitesse3;
    int yAvavitesse3;
    int timervitesse1;
    int timervitesse2;
    int timervitesse3;
    int timervitesse4;
    int timervitesse5;



/*                                         Fin                                               */




/*////////////////////////////////////////Initialisation du jeu///////////////////////////////*/
/*////////////////////////////////////////Initialisation du jeu///////////////////////////////*/


void init_game(){               //mettre votre code d'initialisation ici

/*-----------------------------------------Element de jeux------------------------------------*/

    //initialisation Fenetre

    largFenetre=500;

    //Initialisation de la Balle

    ballx=WINDOW_WIDTH/2;           //Placement de la balle
    bally=WINDOW_HEIGHT-60;
    rayonBall=15;                   //Taille de la balle
    vitesseX = 10;                   //Vitesse de la balle
    vitesseY = -13;

    //Initialisation trainé Balle

    ball2x = ballx-3;
    ball2y = bally +3;
    rayonBall2 = rayonBall-1;
    ball3x = ball2x-3;
    ball3y = ball2y-3;
    rayonBall3 =rayonBall-2;

    //Initialisation de la Barre

    xBarre = WINDOW_WIDTH/2;                   //Placement de la barre
    yBarre = WINDOW_HEIGHT-50;

    longBarre = (WINDOW_WIDTH-(largFenetre*2))/6;      //Forme de la Barre
    hauteurBarre = 15;
    deplTouche = 20;                //Déplacement de la Barre
    couleurBarre = 249;

    //Initialisation de la Barre2

    xBarre2 = WINDOW_WIDTH/2;;                   //Placement de la barre
    yBarre2 = 50;

    longBarre2 = (WINDOW_WIDTH-(largFenetre*2))/6;                  //Forme de la Barre
    hauteurBarre2 = 15;
    deplTouche2 = 20;                //Déplacement de la Barre


    //Initialisation des Briques

    nbcolonne = 7 ;                 //Taille tableau
    nbligne = 5 ;

    xBrique;                        //Placement des Briques
    yBrique;
    espaceBrique = 5;

    longBrique = 100;                //Formes des Briques
    hauteurBrique = 75;

    xEcart = (WINDOW_WIDTH-((longBrique*nbcolonne)+(espaceBrique*(nbcolonne-1))))/2;                    //(WINDOW_WIDTH-((longBrique*nbcolonne)+(espaceBrique*(nbcolonne-1))))/2
    yEcart = 30;

    ytabmulti=210;
    xtabmulti=0;

/*-----------------------------------------Gameplay-------------------------------------------*/

    //*************************Tableau Brique Histoire

    nbBrique=(nbligne*nbcolonne)+(nbcolonne*2)+(2*5);     //Vie Brique
    briqueTouche=0;                                       //Nb brique touché


    nbBriqueMulti=23;  //(nbligne*nbcolonne)+(nbcolonne*2)+(2*5); //Vie Brique Multi
    briqueToucheMulti=0;                                   //Nb brique touché Multi

    //GamePlay

    nbvie=3;                        //Nb de vie possible pour le joueur
    nbvieJ2=3;                        //Nb de vie possible pour le joueur
    tentative=1;                    // Initialisation des tentative
    tentativeJ2=1;                    // Initialisation des tentative
    diminutionBarre=1.5;            // Taille de la diminution de la barre a la perte de vie
    PauseGame=1;                    //Si différent de 1 met le jeu en pause
    reinittab=0;
    timeinittab=0;
    accelPalier=1;
    palierVitesse=0;
    movex=0;
    movey=0;
    moveYorN=0;
    xmoveYorN=0;

    toucheQ=0;
    toucheD=0;
    toucheLEFT=0;
    toucheRIGHT=0;


/*-----------------------------------------Menu-----------------------------------------------*/

    start=0;
    histoire=0;
    arcade=0;
    solo=0;
    multi=0;
    coop=0;
    versus=0;

/*-----------------------------------------Affichage------------------------------------------*/

    //Asset accueil

    longLogo=445;
    hautLogo=50;
    xrectLogo=(WINDOW_WIDTH-longLogo)/2;
    yrectLogo=-100;

    xdebutjeu=-355;
    ydebutjeu=1080;

    //Asset menu

    longmenu= 355;
    xrectMenu=(WINDOW_WIDTH-longmenu)/2;
    menuY=520;
    menuY2=1100;
    menuHistoire=-longmenu;
    menuXarcade=-longmenu;
    menuSolo=-longmenu;
    menuMulti=-longmenu;
    menuCoop=-longmenu;
    menuVersus=-longmenu;
    yavamenu=1083;
    ybonjour=700;

    //Compteur

    compteurBrique1=0;
    compteurBrique2=0;
    compteurBrique3=0;
    compteurBrique4=0;
    compteurBrique5=0;

    compteurPallier1=1;
    compteurPallier2=0;
    compteurPallier3=0;
    compteurPallier4=0;
    compteurPallier5=0;

    CompteurVieJ1=3;
    CompteurVieJ2=3;

    //Ecran GameOver

    xrectGameOver=WINDOW_WIDTH/9;
    yrectGameOver=WINDOW_HEIGHT/4;
    longGameOver=WINDOW_WIDTH-((WINDOW_WIDTH/9)*2);
    hautGameOver=200;

    xDefaite = -500;
    yAvaDefaite = 947;

    xfin1 = -1080;
    xfin2 = -1080;


    //Ecran Victoire

    xrectVictoire=WINDOW_WIDTH/9;
    yrectVictoire=WINDOW_HEIGHT/3;
    longVictoire=WINDOW_WIDTH-((WINDOW_WIDTH/9)*2);
    hautVictoire=200;

    xVictoire = -500;
    yAvavictoire = 947;
    vitVictoire = 25;
    avavitVictoire = 10;
                

            //Mission

    xkitty=510;
    ykitty=-290;
    vitesseAssetX=5;
    vitesseAssetY=5;

            //Asset Jeux

    fond=1080;
    xAvavie=50;
    yAvavie=-130;
    xAvavie2=50;
    yAvavie2=-130;
    vitesseAvavieY=10;
    xAvavitesse=50;
    yAvavitesse=-130;
    xAvavitesse2=50;
    yAvavitesse2=-130;
    xAvavitesse3=50;
    yAvavitesse3=-130;
    timervitesse1=0;
    timervitesse2=0;
    timervitesse3=0;
    timervitesse4=0;
    timervitesse5=0;



}




/*////////////////////////////////////////Initialisation des tableaux/////////////////////////*/
/*////////////////////////////////////////Initialisation des tableaux/////////////////////////*/


void init_tab(){
    //Tab Histoire 1


            for(int i=0;i<nbcolonne;i++){   //Initialisation du tableau des vies
            for(int j=0;j<nbligne;j++){
                if ((j<2 && j>=0)){
                    viebrique[i][j]=2;
                }
                else if ((j==2) && (i>=1) && (i<=5)){
                    viebrique[i][j]=3;
                }
                else {
                    viebrique[i][j]=1;
                }
            }     
        }   

    //Tab Arcade

    for(int i=0;i<nbcolonne;i++){   //Initialisation du tableau des vies
        for(int j=0;j<3;j++){
            if ((j<3 && j>=0) && i==1){
                vieMulti[i][j]=0;
            }
            else if ((j<3 && j>=0) && i==5){
                vieMulti[i][j]=0;
            }


            else if ((j==1) && (i>=2) &&(i<=4)){
                vieMulti[i][j]=3;
            }

            else if (j==1){
                vieMulti[i][j]=2;
            }

            else {
                vieMulti[i][j]=1;
            }
        }
    }     
      
}




/*/////////////////////////////////////////Element de jeux\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
/*/////////////////////////////////////////Element de jeux\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


/*-----------------------------------------BALLE----------------------------------------------*/


void rebondBall(){              //Rebond de la balle sur la fenetre + Diminution de la barre 

    if (ballx >= WINDOW_WIDTH-rayonBall-largFenetre)        //Rebond largeur
        {vitesseX = vitesseX*(-1);
    }

    if (ballx <= rayonBall+largFenetre) 
        {vitesseX = vitesseX*(-1);
    }

    if (bally >= WINDOW_HEIGHT-rayonBall)       //Rebond bas fenetre
        {vitesseY = vitesseY*(-1);
        tentative=tentative+1;                  //Rajoute une tentative
        longBarre=longBarre-((longBarre-(longBarre/diminutionBarre))/2);  //Diminue la taille de la barre lorsque -1 vie joueur
        xBarre=xBarre+((longBarre-(longBarre/diminutionBarre))/2);

        if ((coop>=1) && (versus==0) && (multi>=1)){
                longBarre2=longBarre2-((longBarre2-(longBarre2/diminutionBarre))/2);  //Diminue la taille de la barre lorsque -1 vie joueur
                xBarre2=xBarre2+((longBarre2-(longBarre2/diminutionBarre))/2);
            }  
        }

    if (bally <= rayonBall)                     //Rebond haut fenetre
        {vitesseY = vitesseY*(-1);

            if ((coop>=1) && (versus==0)){
                longBarre2=longBarre2-((longBarre2-(longBarre2/diminutionBarre))/2);  //Diminue la taille de la barre lorsque -1 vie joueur
                xBarre2=xBarre2+((longBarre2-(longBarre2/diminutionBarre))/2);
                tentative=tentative+1;          //Rajoute une tentative
            }                                   
            else if ((versus>=1) && (coop==0)){
                longBarre2=longBarre2-((longBarre2-(longBarre2/diminutionBarre))/2);  //Diminue la taille de la barre lorsque -1 vie joueur
                xBarre2=xBarre2+((longBarre2-(longBarre2/diminutionBarre))/2);
                tentativeJ2=tentativeJ2+1; 
            } 
        }
}



void drawBall(){                //Dessin de la ball + Acceleration
    changeColor(64, 218, 249); 
    drawCircle(ballx,bally,rayonBall);  //Dessin de la ball
    ballx=ballx+ vitesseX;      //Déplacement de la balle largeur
    bally=bally+ vitesseY;      //Déplacement de la balle hauteur
}

/*void drawAccelaration(){
//Accéleration de la balle 
    if((briqueTouche==15) && (palierVitesse==0)){
        if ((briqueTouche==15) && (vitesseX>0)){
            vitesseX=vitesseX+10;
        }

        else if ((briqueTouche==15) && (vitesseX<0)){
            vitesseX=vitesseX-10;
        }


        if ((briqueTouche==15) && (vitesseY>0)){
            vitesseY=vitesseY+10;
        }

        else if ((briqueTouche==15) && (vitesseY<0)){
            vitesseY=vitesseY-10;
        }
        palierVitesse=palierVitesse+1;
    }

//Accéleration de la balle 2

    if((briqueTouche==25) && (palierVitesse==0)){
        if ((briqueTouche==25) && (vitesseX>0)){
            vitesseX=vitesseX+2;
        }

        else if ((briqueTouche==25) && (vitesseX<0)){
            vitesseX=vitesseX-2;
        }


        if ((briqueTouche==25) && (vitesseY>0)){
            vitesseY=vitesseY+2;
        }

        else if ((briqueTouche==25) && (vitesseY<0)){
            vitesseY=vitesseY-2;
        }
        palierVitesse=palierVitesse+1;    
    }
//Accéleration de la balle 3

    if((briqueTouche==40) && (palierVitesse==0)){
        if ((briqueTouche==40) && (vitesseX>0)){
            vitesseX=vitesseX+2;
        }

        else if ((briqueTouche==40) && (vitesseX<0)){
            vitesseX=vitesseX-2;
        }


        if ((briqueTouche==40) && (vitesseY>0)){
            vitesseY=vitesseY+2;
        }

        else if ((briqueTouche==40) && (vitesseY<0)){
            vitesseY=vitesseY-2;
        }
        palierVitesse=palierVitesse+1;
    }
}
*/

void drawAccelarationMulti(){
    //Pallier 2 Vitesse - 12/15

        if((accelPalier==2) && (palierVitesse==0)){
            if ((accelPalier==2) && (vitesseX>0)){
                vitesseX=vitesseX+2;        
            }

            else if ((accelPalier==2) && (vitesseX<0)){
                vitesseX=vitesseX-2;        
            }


            if ((accelPalier==2) && (vitesseY>0)){
                vitesseY=vitesseY+2;
            }

            else if ((accelPalier==2) && (vitesseY<0)){
                vitesseY=vitesseY-2;
            }
            moveYorN=1;
            palierVitesse= palierVitesse+1;
        }

    //Pallier 3 Vitesse - 12/15

        if(accelPalier==3){
            xmoveYorN=1;
            moveYorN=0;

        }

    //Pallier 4 Vitesse - 12/15

        if(accelPalier==4){
            moveYorN=1;
            xmoveYorN=1;
        }

    //Pallier 5 - 14/17

        if((accelPalier==5) && (palierVitesse==1)){
            if ((accelPalier==2) && (vitesseX>0)){
                vitesseX=vitesseX+2;        
            }

            else if ((accelPalier==5) && (vitesseX<0)){
                vitesseX=vitesseX-2;
            }


            if ((accelPalier==5) && (vitesseY>0)){
                vitesseY=vitesseY+2;
            }

            else if ((accelPalier==5) && (vitesseY<0)){
                vitesseY=vitesseY-2;
            }
            palierVitesse= palierVitesse+1;
        }


    //Pallier 6 - 14/17

        if(accelPalier==6){
            xmoveYorN=0;
            moveYorN=1;

        }

    //Pallier 7 - 14/17

        if(accelPalier==7){
            xmoveYorN=0;
            moveYorN=0;

        }

    // Pallier 8 - 16/19

        if((accelPalier==8) && (palierVitesse==2)){

            xmoveYorN=1;
            moveYorN=1;

            if ((accelPalier==8) && (vitesseX>0)){
                vitesseX=vitesseX+2;        
            }

            else if ((accelPalier==8) && (vitesseX<0)){
                vitesseX=vitesseX-2;
            }


            if ((accelPalier==8) && (vitesseY>0)){
                vitesseY=vitesseY+2;
            }

            else if ((accelPalier==8) && (vitesseY<0)){
                vitesseY=vitesseY-2;
            }
            palierVitesse= palierVitesse+1;
        }

    //pallier 9 - 16/19

        if(accelPalier==9){
            xmoveYorN=1;
            moveYorN=0;
        }

    //pallier 10 - 18/21

        if((accelPalier==10) && (palierVitesse==3)){
            if ((accelPalier==10) && (vitesseX>0)){
                vitesseX=vitesseX+2;
            }

            else if ((accelPalier==10) && (vitesseX<0)){
                vitesseX=vitesseX-2;
            }


            if ((accelPalier==10) && (vitesseY>0)){
                vitesseY=vitesseY+2;
            }

            else if ((accelPalier==10) && (vitesseY<0)){
                vitesseY=vitesseY-2;
            }
            palierVitesse= palierVitesse+1;
        }

    //pallier 11 - 18/21

        if(accelPalier==11){
            xmoveYorN=1;
            moveYorN=1;
        }

    //pallier 12 - 18/21

        if((accelPalier==12) && (palierVitesse==4)){
            if ((accelPalier==12) && (vitesseX>0)){
                vitesseX=vitesseX+2;
            }

            else if ((accelPalier==12) && (vitesseX<0)){
                vitesseX=vitesseX-2;
            }


            if ((accelPalier==12) && (vitesseY>0)){
                vitesseY=vitesseY+2;
            }

            else if ((accelPalier==12) && (vitesseY<0)){
                vitesseY=vitesseY-2;
            }
            palierVitesse= palierVitesse+1;
        }

    //pallier 13 - 18/21

        if(accelPalier==13){
            xmoveYorN=0;
            moveYorN=0;
        }

    //pallier 14 - 18/21

        if(accelPalier==14){
            xmoveYorN=1;
            moveYorN=1;
        }

    //pallier 15 - 20/23

        if((accelPalier==15) && (palierVitesse==5)){
            if ((accelPalier==15) && (vitesseX>0)){
                vitesseX=vitesseX+2;
            }

            else if ((accelPalier==15) && (vitesseX<0)){
                vitesseX=vitesseX-2;
            }


            if ((accelPalier==15) && (vitesseY>0)){
                vitesseY=vitesseY+2;
            }

            else if ((accelPalier==15) && (vitesseY<0)){
                vitesseY=vitesseY-2;
            }
            palierVitesse= palierVitesse+1;
        }

    //pallier 16 - 20/23

        if(accelPalier==16){
            xmoveYorN=0;
            moveYorN=0;
        }

    //pallier 17 - 22/25

        if((accelPalier==15) && (palierVitesse==6)){
            if ((accelPalier==15) && (vitesseX>0)){
                vitesseX=vitesseX+2;
            }

            else if ((accelPalier==15) && (vitesseX<0)){
                vitesseX=vitesseX-2;
            }


            if ((accelPalier==15) && (vitesseY>0)){
                vitesseY=vitesseY+2;
            }

            else if ((accelPalier==15) && (vitesseY<0)){
                vitesseY=vitesseY-2;
            }
            palierVitesse= palierVitesse+1;
        }

    //pallier 19 - 24/27

        if((accelPalier==15) && (palierVitesse==7)){
            if ((accelPalier==15) && (vitesseX>0)){
                vitesseX=vitesseX+2;
            }

            else if ((accelPalier==15) && (vitesseX<0)){
                vitesseX=vitesseX-2;
            }


            if ((accelPalier==15) && (vitesseY>0)){
                vitesseY=vitesseY+2;
            }

            else if ((accelPalier==15) && (vitesseY<0)){
                vitesseY=vitesseY-2;
            }
            palierVitesse= palierVitesse+1;
        }

    //pallier 21 - 26/29

        if((accelPalier==15) && (palierVitesse==8)){
            if ((accelPalier==15) && (vitesseX>0)){
                vitesseX=vitesseX+2;
            }

            else if ((accelPalier==15) && (vitesseX<0)){
                vitesseX=vitesseX-2;
            }


            if ((accelPalier==15) && (vitesseY>0)){
                vitesseY=vitesseY+2;
            }

            else if ((accelPalier==15) && (vitesseY<0)){
                vitesseY=vitesseY-2;
            }
            palierVitesse= palierVitesse+1;
        }



}

void drawBalltraine(){                //Dessin de la traine
    changeColor(133, 231, 251);
    drawCircle(ball2x,ball2y,rayonBall2);  //Dessin de la traine
    ball2x=ballx*1.001;      //Déplacement de la balle largeur
    ball2y=bally*1.001;      //Déplacement de la balle hauteur
}
void drawBalltraine2(){ 
    changeColor(185, 240, 251);
    drawCircle(ball3x,ball3y,rayonBall3);  //Dessin de la traine
    ball3x=ball2x*1.001;      //Déplacement de la balle largeur
    ball3y=ball2y*1.001;      //Déplacement de la balle hauteur
}



/*-----------------------------------------BARRE---------------------------------------------*/


void drawBarre(){                   //Dessin de la barre et changement de couleur


    if(tentative==1){
        changeColor(114, 246, 82);
        drawRect(xBarre,yBarre,longBarre,hauteurBarre);

    }
    else if(tentative==2){
        changeColor(246, 246, 82);
        drawRect(xBarre,yBarre,longBarre,hauteurBarre);

    }
    else if(tentative==3){
        changeColor(246, 82, 82);
        drawRect(xBarre,yBarre,longBarre,hauteurBarre);
    }

    drawRect(xBarre+((longBarre/2)-2),yBarre,4,hauteurBarre);


}
void drawBarre2(){                  //Dessin de la barre Coop et changement de couleur

    if(tentative==1){
        changeColor(114, 246, 82);
        drawRect(xBarre2,yBarre2,longBarre,hauteurBarre);

    }
    else if(tentative==2){
        changeColor(246, 246, 82);
        drawRect(xBarre2,yBarre2,longBarre,hauteurBarre);

    }
    else if(tentative==3){
        changeColor(246, 82, 82);
        drawRect(xBarre2,yBarre2,longBarre,hauteurBarre);
    }


}
void drawBarreVersus(){              //Dessin de la barre Versus et changement de couleur

    if(tentativeJ2==1){
        changeColor(114, 246, 82);
        drawRect(xBarre2,yBarre2,longBarre2,hauteurBarre2);

    }
    else if(tentativeJ2==2){
        changeColor(246, 246, 82);
        drawRect(xBarre2,yBarre2,longBarre2,hauteurBarre2);

    }
    else if(tentativeJ2==3){
        changeColor(246, 82, 82);
        drawRect(xBarre2,yBarre2,longBarre2,hauteurBarre2);
    }


}

void rebondBarre (){            //Rebond de la barre
    
                    //haut

                if(((bally>=yBarre-rayonBall) && (bally<yBarre)) &&
                    ((ballx<=xBarre+(longBarre/2)) && (ballx>=xBarre)) && (vitesseY>0) && (vitesseX>0)){
                    vitesseY=vitesseY*(-1);
                    vitesseX=vitesseX*(-1);
                }

                else if(((bally>=yBarre-rayonBall) && (bally<yBarre)) &&
                    ((ballx<=xBarre+(longBarre/2)) && (ballx>=xBarre)) && (vitesseY>0) && (vitesseX<0)){
                    vitesseY=vitesseY*(-1);
                }

                else if(((bally>=yBarre-rayonBall) && (bally<yBarre)) &&
                    ((ballx<=xBarre+longBarre) && (ballx>=xBarre+(longBarre/2))) && (vitesseY>0) && (vitesseX<0)){
                    vitesseY=vitesseY*(-1);
                    vitesseX=vitesseX*(-1);
                }

                else if(((bally>=yBarre-rayonBall) && (bally<yBarre)) &&
                    ((ballx<=xBarre+longBarre) && (ballx>=xBarre+(longBarre/2))) && (vitesseY>0) && (vitesseX>0)){
                    vitesseY=vitesseY*(-1);
                }



                    //gauche

                if((ballx>=xBarre-rayonBall) && (ballx<xBarre) &&
                    ((bally>=yBarre) && (bally<=yBarre+hauteurBarre)) && (vitesseX>0)){
                    vitesseX=vitesseX*(-1);
                    
                }
                    //droite

                else if((ballx<=xBarre+longBarre+rayonBall) && (ballx>xBarre+longBarre) &&
                    ((bally>=yBarre) && (bally<=yBarre+hauteurBarre)) && (vitesseX<0)){
                    vitesseX=vitesseX*(-1);
                }

}
void rebondBarre2(){            //Rebond de la barre
    
                    //bas

                if(((bally<=yBarre2+hauteurBarre2+rayonBall) && (bally>yBarre2)) &&
                    ((ballx<=xBarre2+(longBarre2/2)) && (ballx>=xBarre2)) && (vitesseY<0) && (vitesseX>0)){
                    vitesseY=vitesseY*(-1);
                    vitesseX=vitesseX*(-1);
                }

                else if(((bally<=yBarre2+hauteurBarre2+rayonBall) && (bally>yBarre2)) &&
                    ((ballx<=xBarre2+(longBarre2/2)) && (ballx>=xBarre2)) && (vitesseY<0) && (vitesseX<0)){
                    vitesseY=vitesseY*(-1);
                }

                else if(((bally<=yBarre2+hauteurBarre2+rayonBall) && (bally>yBarre2)) &&
                    ((ballx<=xBarre2+longBarre2) && (ballx>=xBarre2+(longBarre2/2))) && (vitesseY<0) && (vitesseX<0)){
                    vitesseY=vitesseY*(-1);
                    vitesseX=vitesseX*(-1);
                }

                else if(((bally<=yBarre2+hauteurBarre2+rayonBall) && (bally>yBarre2)) &&
                    ((ballx<=xBarre2+longBarre2) && (ballx>=xBarre2+(longBarre2/2))) && (vitesseY<0) && (vitesseX>0)){
                    vitesseY=vitesseY*(-1);
                }


                    //gauche

                if((ballx>=xBarre2-rayonBall) && (ballx<xBarre2) &&
                    ((bally>=yBarre2) && (bally<=yBarre2+hauteurBarre2)) && (vitesseX>0)){
                    vitesseX=vitesseX*(-1);
                    
                }
                    //droite

                else if((ballx<=xBarre2+longBarre2+rayonBall) && (ballx>xBarre2+longBarre2) &&
                    ((bally>=yBarre2) && (bally<=yBarre2+hauteurBarre2)) && (vitesseX<0)){
                    vitesseX=vitesseX*(-1);
                }

}



/*-----------------------------------------Brique---------------------------------------------*/


void drawBrick(){              //Dessin des briques, Rebond des briques, Vie et Brique touché


    for(i=0;i<nbcolonne;i++){               //Correspond aux colonnes de mes briques
        for (j=0; j<nbligne;j++){           //Correspond aux lignes de mes briques
            if (viebrique[i][j]>=1){
                if(viebrique[i][j]<2){
                    changeColor(64, 218, 249);
                }

                else if (viebrique[i][j]==2){
                    changeColor(167, 19, 255);
                }

                else if (viebrique[i][j]==3){
                    changeColor(246, 101, 250);
                }
                xBrique=xEcart+(longBrique+espaceBrique)*i;
                yBrique=yEcart+(hauteurBrique+espaceBrique)*j;
                drawRect(xBrique,yBrique,longBrique,hauteurBrique);


                             //Rebond de la balle sur les Briques
                            
                    //bas
                
                if(((bally<=yBrique+hauteurBrique+rayonBall) && (bally>yBrique+hauteurBrique)) &&
                    ((ballx<=xBrique+longBrique) && (ballx>=xBrique)) && (vitesseY<0)){
                    vitesseY=vitesseY*(-1); 
                    viebrique[i][j]=viebrique[i][j]-1;
                    briqueTouche=briqueTouche+1;
                }

                    //haut

                else if(((bally>=yBrique-rayonBall) && (bally<yBrique)) &&
                    ((ballx<=xBrique+longBrique) && (ballx>=xBrique)) && (vitesseY>0)){
                    vitesseY=vitesseY*(-1);
                    viebrique[i][j]=viebrique[i][j]-1;
                    briqueTouche=briqueTouche+1;
                }

                    //gauche

                if((ballx>=xBrique-rayonBall) && (ballx<xBrique) &&
                    ((bally>=yBrique) && (bally<=yBrique+hauteurBrique)) && (vitesseX>0)){
                    vitesseX=vitesseX*(-1);
                    viebrique[i][j]=viebrique[i][j]-1;
                    briqueTouche=briqueTouche+1;
                }

                    //droite

                else if((ballx<=xBrique+longBrique+rayonBall) && (ballx>xBrique+longBrique) &&
                    ((bally>=yBrique) && (bally<=yBrique+hauteurBrique)) && (vitesseX<0)){
                    vitesseX=vitesseX*(-1);
                    viebrique[i][j]=viebrique[i][j]-1; 
                    briqueTouche=briqueTouche+1;
                }
                
            }
        }
    }

}

void drawBrickMulti(){         //Dessin des briques, Rebond des briques, Vie et Brique touché


    for(i=0;i<nbcolonne;i++){               //Correspond aux colonnes de mes briques
        for (j=0; j<nbligne;j++){           //Correspond aux lignes de mes briques
            if (vieMulti[i][j]>=1){
                if(vieMulti[i][j]<2){
                    changeColor(64, 218, 249);
                }

                else if (vieMulti[i][j]==2){
                    changeColor(167, 19, 255);
                }

                else if (vieMulti[i][j]==3){
                    changeColor(246, 101, 250);
                }
                xBrique=(xEcart+xtabmulti)+(longBrique+espaceBrique)*i;
                yBrique=(yEcart+ytabmulti)+(hauteurBrique+espaceBrique)*j;
                drawRect(xBrique,yBrique,longBrique,hauteurBrique);


                             //Rebond de la balle sur les Briques
                            
                    //bas
                
                if(((bally<=yBrique+hauteurBrique+rayonBall) && (bally>yBrique+hauteurBrique)) &&
                    ((ballx<=xBrique+longBrique) && (ballx>=xBrique)) && (vitesseY<0)){
                    vitesseY=vitesseY*(-1); 
                    vieMulti[i][j]=vieMulti[i][j]-1;
                    briqueToucheMulti=briqueToucheMulti+1;
                    compteurBrique1=compteurBrique1+1;
                }

                    //haut

                else if(((bally>=yBrique-rayonBall) && (bally<yBrique)) &&
                    ((ballx<=xBrique+longBrique) && (ballx>=xBrique)) && (vitesseY>0)){
                    vitesseY=vitesseY*(-1);
                    vieMulti[i][j]=vieMulti[i][j]-1;
                    briqueToucheMulti=briqueToucheMulti+1;
                    compteurBrique1=compteurBrique1+1;

                }

                    //gauche

                if((ballx>=xBrique-rayonBall) && (ballx<xBrique) &&
                    ((bally>=yBrique) && (bally<=yBrique+hauteurBrique)) && (vitesseX>0)){
                    vitesseX=vitesseX*(-1);
                    vieMulti[i][j]=vieMulti[i][j]-1;
                    briqueToucheMulti=briqueToucheMulti+1;
                    compteurBrique1=compteurBrique1+1;
                }

                    //droite

                else if((ballx<=xBrique+longBrique+rayonBall) && (ballx>xBrique+longBrique) &&
                    ((bally>=yBrique) && (bally<=yBrique+hauteurBrique)) && (vitesseX<0)){
                    vitesseX=vitesseX*(-1);
                    vieMulti[i][j]=vieMulti[i][j]-1; 
                    briqueToucheMulti=briqueToucheMulti+1;
                    compteurBrique1=compteurBrique1+1;

                }
                
            }
        }
    }

}

void reninitpalier(){          //Mode arcade reinitialise les briques et change de palier
        if((briqueToucheMulti>=nbBriqueMulti) && (tentative<=nbvie)){
            reinittab=1;
            timeinittab=timeinittab+1;
                if((reinittab==1) && (timeinittab>=20)){
                    init_tab();
                    reinittab=0;
                    timeinittab=0;            
                    briqueToucheMulti=0;
                    accelPalier=accelPalier+1;
                    compteurPallier1=compteurPallier1+1;
                }
        }
}

void moveBricky(){
    if ((ytabmulti<260) && (movey==0) && (moveYorN==1)){
        ytabmulti=ytabmulti+2;
    }
    else if (ytabmulti>=260 && (moveYorN==1)){
        movey=1;
    }
    if ((ytabmulti>160) && (movey==1) && (moveYorN==1)){
        ytabmulti=ytabmulti-2;
    }
    else if ((ytabmulti<=160) && (moveYorN==1)){
        movey=0;
    }
    if (moveYorN == 0){
        ytabmulti = 210;
    }
}

void moveBrickx(){
    if ((xtabmulti<30) && (movex==0) && (xmoveYorN==1)){
        xtabmulti=xtabmulti+2;
    }
    else if (xtabmulti>=30 && (xmoveYorN==1)){
        movex=1;
    }
    if ((xtabmulti>-30) && (movex==1) && (xmoveYorN==1)){
        xtabmulti=xtabmulti-2;
    }
    else if ((xtabmulti<=-30) && (xmoveYorN==1)){
        movex=0;
    }
    if (xmoveYorN == 0){
        xtabmulti = 0;
    }
}

void moveBrickmultiy(){
    if ((ytabmulti<420) && (movey==0) && (moveYorN==1)){
        ytabmulti=ytabmulti+2;
    }
    else if (ytabmulti>=420 && (moveYorN==1)){
        movey=1;
    }
    if ((ytabmulti>360) && (movey==1) && (moveYorN==1)){
        ytabmulti=ytabmulti-2;
    }
    else if ((ytabmulti<=360) && (moveYorN==1)){
        movey=0;
    }
    if (moveYorN == 0){
        ytabmulti = 390;
    }

}




/*/////////////////////////////////////////Menu\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
/*/////////////////////////////////////////Menu\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

/*-----------------------------------------Total-----------------------------------*/

void drawAll (){
    sprite(0,0, "Asset/gauche.bmp");
    sprite(0,1073, "Asset/borduregauche.bmp");
    sprite(1420,0, "Asset/droite.bmp");
    sprite(1420,1070, "Asset/borduredroite.bmp");
}

/*-----------------------------------------Ecran d'accueil-----------------------------------*/

void drawAccueil(){
    sprite(0, 0,"Asset/accueil.bmp");

    sprite(615, yrectLogo,"Asset/logo2.bmp");
    sprite(746, ydebutjeu,"Asset/start2.bmp");


    if(yrectLogo<100){
        yrectLogo=yrectLogo-vitesseY;
        }

    if(ydebutjeu>800){
        ydebutjeu=ydebutjeu-avavitVictoire;
        }

}

/*-----------------------------------------Choix de mode de jeux-----------------------------*/

void drawMenu(){
    sprite(0, 0,"Asset/accueil.bmp");
    sprite(746, ydebutjeu,"Asset/start2.bmp");
    sprite(615, yrectLogo,"Asset/logo2.bmp");
    sprite(40, menuY2,"Asset/newmenu.bmp");
    sprite(1430, ybonjour,"Asset/bonjour.bmp");
    sprite(1420, 693,"Asset/error.bmp");
    sprite(1420, yavamenu,"Asset/ava.bmp");
    sprite(1420,1070, "Asset/borduredroite.bmp");
    sprite(0,1073, "Asset/borduregauche.bmp");

    


    if(ydebutjeu<1100){
        ydebutjeu=ydebutjeu+avavitVictoire;
        }

    if(menuY2>500){
        menuY2=menuY2-avavitVictoire;
    }  

    if(yavamenu>703){
        yavamenu=yavamenu-avavitVictoire;
    }

    if(ybonjour>60){
        ybonjour=ybonjour-avavitVictoire;
    }
}

/*-----------------------------------------Choix Arcade-----------------------------*/
/*
void drawMenuarcade(){
    sprite(0, 0,"Asset/menuarcade.bmp");

    sprite(xrectLogo, yrectLogo,"Asset/logo.bmp");
    sprite(menuHistoire, menuY,"Asset/mhistoire.bmp");
    sprite(menuXarcade, menuY2,"Asset/marcade.bmp");
    sprite(menuSolo, menuY,"Asset/msolo.bmp");
    sprite(menuMulti, menuY2,"Asset/mmulti.bmp");

    if(yrectLogo>-200){
        yrectLogo=yrectLogo+vitesseY;
        }

    if(menuHistoire<4000){
        menuHistoire=menuHistoire+avavitVictoire;
    }  

    if(menuXarcade<4000){
        menuXarcade=menuXarcade+avavitVictoire;
    } 

    if(menuSolo<xrectMenu){
        menuSolo=menuSolo+avavitVictoire;
    }  

    if(menuMulti<xrectMenu){
        menuMulti=menuMulti+avavitVictoire;
    } 
}

void drawMenumulti(){


    sprite(0, 0,"Asset/menuarcade.bmp");
    sprite(menuCoop, menuY,"Asset/mcoop.bmp");
    sprite(menuVersus, menuY2,"Asset/mversus.bmp");
    sprite(menuSolo, menuY,"Asset/msolo.bmp");
    sprite(menuMulti, menuY2,"Asset/mmulti.bmp");

 
    if(menuCoop<xrectMenu){
        menuCoop=menuCoop+avavitVictoire;
    }  

    if(menuVersus<xrectMenu){
        menuVersus=menuVersus+avavitVictoire;
    } 

    if(menuSolo<4000){
        menuSolo=menuSolo+avavitVictoire;
    }  

    if(menuMulti<4000){
        menuMulti=menuMulti+avavitVictoire;
    } 
}
*/


/*/////////////////////////////////////////Mode Arcade Commun\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
/*/////////////////////////////////////////Mode Arcade Commun\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

void drawScore(){
    sprite(7, 7,"Asset/score.bmp");
}

void drawScoreVersus(){
    sprite(7, 7,"Asset/score2.bmp");
}

void drawCompteur(){
    if (compteurBrique1 == 0){
        sprite(375, 374,"Asset/0.bmp");
    }

        else if (compteurBrique1 == 1){
            sprite(375, 374,"Asset/11.bmp");
        }

        else if (compteurBrique1 == 2){
            sprite(375, 374,"Asset/22.bmp");
        }

        else if (compteurBrique1 == 3){
            sprite(375, 374,"Asset/33.bmp");
        }

        else if (compteurBrique1 == 4){
            sprite(375, 374,"Asset/44.bmp");
        }

        else if (compteurBrique1 == 5){
            sprite(375, 374,"Asset/55.bmp");
        }

        else if (compteurBrique1 == 6){
            sprite(375, 374,"Asset/66.bmp");
        }

        else if (compteurBrique1 == 7){
            sprite(375, 374,"Asset/77.bmp");
        }

        else if (compteurBrique1 == 8){
            sprite(375, 374,"Asset/88.bmp");
        }

        else if (compteurBrique1 == 9){
            sprite(375, 374,"Asset/99.bmp");
        }

        else if (compteurBrique1 == 10){
            compteurBrique1 = 0;
            compteurBrique2 = compteurBrique2+1;
        }



    if (compteurBrique2 == 0){
        sprite(299, 374,"Asset/0.bmp");
    }
        else if (compteurBrique2 == 1){
            sprite(299, 374,"Asset/11.bmp");
        }

        else if (compteurBrique2 == 2){
            sprite(299, 374,"Asset/22.bmp");
        }

        else if (compteurBrique2 == 3){
            sprite(299, 374,"Asset/33.bmp");
        }

        else if (compteurBrique2 == 4){
            sprite(299, 374,"Asset/44.bmp");
        }

        else if (compteurBrique2 == 5){
            sprite(299, 374,"Asset/55.bmp");
        }

        else if (compteurBrique2 == 6){
            sprite(299, 374,"Asset/66.bmp");
        }

        else if (compteurBrique2 == 7){
            sprite(299, 374,"Asset/77.bmp");
        }

        else if (compteurBrique2 == 8){
            sprite(299, 374,"Asset/88.bmp");
        }

        else if (compteurBrique2 == 9){
            sprite(299, 374,"Asset/99.bmp");
        }

        else if (compteurBrique2 == 10){
            compteurBrique2 = 0;
            compteurBrique3 = compteurBrique3+1;
        }



    if (compteurBrique3 == 0){
        sprite(221, 374,"Asset/0.bmp");
    }
        else if (compteurBrique3 == 1){
            sprite(221, 374,"Asset/11.bmp");
        }

        else if (compteurBrique3 == 2){
            sprite(221, 374,"Asset/22.bmp");
        }

        else if (compteurBrique3 == 3){
            sprite(221, 374,"Asset/33.bmp");
        }

        else if (compteurBrique3 == 4){
            sprite(221, 374,"Asset/44.bmp");
        }

        else if (compteurBrique3 == 5){
            sprite(221, 374,"Asset/55.bmp");
        }

        else if (compteurBrique3 == 6){
            sprite(221, 374,"Asset/66.bmp");
        }

        else if (compteurBrique3 == 7){
            sprite(221, 374,"Asset/77.bmp");
        }

        else if (compteurBrique3 == 8){
            sprite(221, 374,"Asset/88.bmp");
        }

        else if (compteurBrique3 == 9){
            sprite(221, 374,"Asset/99.bmp");
        }

        else if (compteurBrique3 == 10){
            compteurBrique3 = 0;
            compteurBrique4 = compteurBrique4+1;
        }



    if (compteurBrique4 == 0){
        sprite(145, 374,"Asset/0.bmp");
    }
        else if (compteurBrique4 == 1){
            sprite(145, 374,"Asset/11.bmp");
        }

        else if (compteurBrique4 == 2){
            sprite(145, 374,"Asset/22.bmp");
        }

        else if (compteurBrique4 == 3){
            sprite(145, 374,"Asset/33.bmp");
        }

        else if (compteurBrique4 == 4){
            sprite(145, 374,"Asset/44.bmp");
        }

        else if (compteurBrique4 == 5){
            sprite(145, 374,"Asset/55.bmp");
        }

        else if (compteurBrique4 == 6){
            sprite(145, 374,"Asset/66.bmp");
        }

        else if (compteurBrique4 == 7){
            sprite(145, 374,"Asset/77.bmp");
        }

        else if (compteurBrique4 == 8){
            sprite(145, 374,"Asset/88.bmp");
        }

        else if (compteurBrique4 == 9){
            sprite(145, 374,"Asset/99.bmp");
        }

        else if (compteurBrique4 == 10){
            compteurBrique4 = 0;
            compteurBrique5 = compteurBrique5+1;
        }



    if (compteurBrique5 == 0){
        sprite(69, 374,"Asset/0.bmp");
    }
        else if (compteurBrique5 == 1){
            sprite(69, 374,"Asset/11.bmp");
        }

        else if (compteurBrique5 == 2){
            sprite(69, 374,"Asset/22.bmp");
        }

        else if (compteurBrique5 == 3){
            sprite(69, 374,"Asset/33.bmp");
        }

        else if (compteurBrique5 == 4){
            sprite(69, 374,"Asset/44.bmp");
        }

        else if (compteurBrique5 == 5){
            sprite(69, 374,"Asset/55.bmp");
        }

        else if (compteurBrique5 == 6){
            sprite(69, 374,"Asset/66.bmp");
        }

        else if (compteurBrique5 == 7){
            sprite(69, 374,"Asset/77.bmp");
        }

        else if (compteurBrique5 == 8){
            sprite(69, 374,"Asset/88.bmp");
        }

        else if (compteurBrique5 == 9){
            sprite(69, 374,"Asset/99.bmp");
        }

}


void drawPalier(){
    if (compteurPallier1 == 0){
        sprite(375, 185,"Asset/0.bmp");
    }

        else if (compteurPallier1 == 1){
            sprite(375, 185,"Asset/11.bmp");
        }

        else if (compteurPallier1 == 2){
            sprite(375, 185,"Asset/22.bmp");
        }

        else if (compteurPallier1 == 3){
            sprite(375, 185,"Asset/33.bmp");
        }

        else if (compteurPallier1 == 4){
            sprite(375, 185,"Asset/44.bmp");
        }

        else if (compteurPallier1 == 5){
            sprite(375, 185,"Asset/55.bmp");
        }

        else if (compteurPallier1 == 6){
            sprite(375, 185,"Asset/66.bmp");
        }

        else if (compteurPallier1 == 7){
            sprite(375, 185,"Asset/77.bmp");
        }

        else if (compteurPallier1 == 8){
            sprite(375, 185,"Asset/88.bmp");
        }

        else if (compteurPallier1 == 9){
            sprite(375, 185,"Asset/99.bmp");
        }

        else if (compteurPallier1 == 10){
            compteurPallier1 = 0;
            compteurPallier2 = compteurPallier2+1;
        }



    if (compteurPallier2 == 0){
        sprite(299, 185,"Asset/0.bmp");
    }
        else if (compteurPallier2 == 1){
            sprite(299, 185,"Asset/11.bmp");
        }

        else if (compteurPallier2 == 2){
            sprite(299, 185,"Asset/22.bmp");
        }

        else if (compteurPallier2 == 3){
            sprite(299, 185,"Asset/33.bmp");
        }

        else if (compteurPallier2 == 4){
            sprite(299, 185,"Asset/44.bmp");
        }

        else if (compteurPallier2 == 5){
            sprite(299, 185,"Asset/55.bmp");
        }

        else if (compteurPallier2 == 6){
            sprite(299, 185,"Asset/66.bmp");
        }

        else if (compteurPallier2 == 7){
            sprite(299, 185,"Asset/77.bmp");
        }

        else if (compteurPallier2 == 8){
            sprite(299, 185,"Asset/88.bmp");
        }

        else if (compteurPallier2 == 9){
            sprite(299, 185,"Asset/99.bmp");
        }

        else if (compteurPallier2 == 10){
            compteurPallier2 = 0;
            compteurPallier3 = compteurPallier3+1;
        }



    if (compteurPallier3 == 0){
        sprite(221, 185,"Asset/0.bmp");
    }
        else if (compteurPallier3 == 1){
            sprite(221, 185,"Asset/11.bmp");
        }

        else if (compteurPallier3 == 2){
            sprite(221, 185,"Asset/22.bmp");
        }

        else if (compteurPallier3 == 3){
            sprite(221, 185,"Asset/33.bmp");
        }

        else if (compteurPallier3 == 4){
            sprite(221, 185,"Asset/44.bmp");
        }

        else if (compteurPallier3 == 5){
            sprite(221, 185,"Asset/55.bmp");
        }

        else if (compteurPallier3 == 6){
            sprite(221, 185,"Asset/66.bmp");
        }

        else if (compteurPallier3 == 7){
            sprite(221, 185,"Asset/77.bmp");
        }

        else if (compteurPallier3 == 8){
            sprite(221, 185,"Asset/88.bmp");
        }

        else if (compteurPallier3 == 9){
            sprite(221, 185,"Asset/99.bmp");
        }

        else if (compteurPallier3 == 10){
            compteurPallier3 = 0;
            compteurPallier4 = compteurPallier4+1;
        }



    if (compteurPallier4 == 0){
        sprite(145, 185,"Asset/0.bmp");
    }
        else if (compteurPallier4 == 1){
            sprite(145, 185,"Asset/11.bmp");
        }

        else if (compteurPallier4 == 2){
            sprite(145, 185,"Asset/22.bmp");
        }

        else if (compteurPallier4 == 3){
            sprite(145, 185,"Asset/33.bmp");
        }

        else if (compteurPallier4 == 4){
            sprite(145, 185,"Asset/44.bmp");
        }

        else if (compteurPallier4 == 5){
            sprite(145, 185,"Asset/55.bmp");
        }

        else if (compteurPallier4 == 6){
            sprite(145, 185,"Asset/66.bmp");
        }

        else if (compteurPallier4 == 7){
            sprite(145, 185,"Asset/77.bmp");
        }

        else if (compteurPallier4 == 8){
            sprite(145, 185,"Asset/88.bmp");
        }

        else if (compteurPallier4 == 9){
            sprite(145, 185,"Asset/99.bmp");
        }

        else if (compteurPallier4 == 10){
            compteurPallier4 = 0;
            compteurPallier5 = compteurPallier5+1;
        }



    if (compteurPallier5 == 0){
        sprite(69, 185,"Asset/0.bmp");
    }
        else if (compteurPallier5 == 1){
            sprite(69, 185,"Asset/11.bmp");
        }

        else if (compteurPallier5 == 2){
            sprite(69, 185,"Asset/22.bmp");
        }

        else if (compteurPallier5 == 3){
            sprite(69, 185,"Asset/33.bmp");
        }

        else if (compteurPallier5 == 4){
            sprite(69, 185,"Asset/44.bmp");
        }

        else if (compteurPallier5 == 5){
            sprite(69, 185,"Asset/55.bmp");
        }

        else if (compteurPallier5 == 6){
            sprite(69, 185,"Asset/66.bmp");
        }

        else if (compteurPallier5 == 7){
            sprite(69, 185,"Asset/77.bmp");
        }

        else if (compteurPallier5 == 8){
            sprite(69, 185,"Asset/88.bmp");
        }

        else if (compteurPallier5 == 9){
            sprite(69, 185,"Asset/99.bmp");
        }
}


void drawCompteurVie(){
    if (versus >= 1) {

        sprite(299, 565,"Asset/0.bmp");
        sprite(69, 565,"Asset/0.bmp");

        if (tentative == 1){
            sprite(375, 565,"Asset/33.bmp");
        }

        else if (tentative == 2){
            sprite(375, 565,"Asset/22.bmp");
        }

        else if (tentative == 3){
            sprite(375, 565,"Asset/11.bmp");
        }

        else if (tentative >= 4){
            sprite(375, 565,"Asset/0.bmp");
        }

        if (tentativeJ2 == 1){
            sprite(145, 565,"Asset/33.bmp");
        }

        else if (tentativeJ2 == 2){
            sprite(145, 565,"Asset/22.bmp");
        }

        else if (tentativeJ2 == 3){
            sprite(145, 565,"Asset/11.bmp");
        }

        else if (tentativeJ2 >= 4){
            sprite(145, 565,"Asset/0.bmp");
        }
    }
}


void drawBackground(){
    sprite(0, 0,"Asset/jeu.bmp");
    sprite(1420, 693,"Asset/avajeux.bmp");
}

/*void drawBackgroundjeu(){
    sprite(500, fond,"Asset/play.bmp");

    if ((solo >= 1) || (versus >=1) || (coop >=2)) {
        fond = fond + 5;
        if (fond <= 0){
            fond == -1080;
        }
    }
}*/


/*/////////////////////////////////////////Mode Arcade Solo\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
/*/////////////////////////////////////////Mode Arcade Solo\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


void drawExplisolo(){
            drawBackground();
            drawScore();
            drawCompteur();
            drawPalier();
            drawBrickMulti();  
            drawBarre();
            sprite(1430, 60,"Asset/avasolo.bmp");
}

void drawFinarcade(){   

    sprite(1430, 60,"Asset/defaitesolo.bmp");

}




/*/////////////////////////////////////////Mode Arcade Coop\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
/*/////////////////////////////////////////Mode Arcade Coop\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


void drawExpliCoop(){
            drawBackground();
            drawScore();
            drawCompteur();
            drawPalier();
            drawBrickMulti();  
            drawBarre();
            drawBarre2();
            sprite(1430, 60,"Asset/avacoop.bmp");
}

//drawFinarcade == drawfinarcadesolo (voir Mode Arcade Solo)





/*/////////////////////////////////////////Mode Arcade Versus\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
/*/////////////////////////////////////////Mode Arcade Versus\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


void drawExpliVersus(){
            drawBackground();
            drawScoreVersus();
            drawCompteur();
            drawPalier();
            drawBrickMulti();  
            drawBarre();
            drawBarre2();
            sprite(1430, 60,"Asset/avaversus.bmp");
}

void drawFinversus(){   

    sprite(1430, 60,"Asset/defaiteJ2.bmp");
    sprite(1430, 60,"Asset/defaiteJ1.bmp");

    if ((tentativeJ2>nbvie) && (xfin1>60)){
        xfin1=xfin1-avavitVictoire;
    }

    else if ((tentative>nbvie) && (xfin2>60)){
        xfin2=xfin2-avavitVictoire;
    }


}


/*/////////////////////////////////////////Mode Histoire\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
/*/////////////////////////////////////////Mode Histoire\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


/*-----------------------------------------Ecran de mission----------------------------------*/
/*
void drawMission(){
    sprite(0, 0,"Asset/fondmission.bmp");
    sprite(20, ykitty,"Asset/kitty2.bmp");
    sprite(xkitty, 180,"Asset/kitty.bmp");
    sprite(0, 0,"Asset/bordurefondmission.bmp");

        if(xkitty>270){
            xkitty=xkitty-vitesseAssetX;
        }
        if(ykitty<80){
            ykitty=ykitty+vitesseAssetY;
        }
}
*/

/*-----------------------------------------GameOver------------------------------------------*/

void drawGameOver(){
    sprite(xDefaite, 225,"Asset/defaite.bmp");       //225
    sprite(0, yAvaDefaite,"Asset/avadefaite.bmp");     //453
        if (xDefaite<0){
            xDefaite=xDefaite+vitVictoire;
        }

        if (yAvaDefaite>453){
            yAvaDefaite=yAvaDefaite-avavitVictoire;
        }
        
}


/*-----------------------------------------Victoire------------------------------------------*/

void drawVictoire(){
    sprite(xVictoire, 225,"Asset/victoire.bmp");       //225
    sprite(0, yAvavictoire,"Asset/avavictoire.bmp");     //453
        if (xVictoire<0){
            xVictoire=xVictoire+vitVictoire;
        }

        if (yAvavictoire>453){
            yAvavictoire=yAvavictoire-avavitVictoire;
        }

}

void drawEnding(){
    sprite(0, 0,"Asset/fin.bmp");
}

/*-----------------------------------------Background-----------------------------------------*/


void drawAva(){                 // Apparition d'Ava dans le jeu

            sprite(xAvavie, yAvavie,"Asset/avavie.bmp");

                if(((yAvavie>=-130) && (yAvavie<=0)) && (tentative==2) && (timervitesse1<50)){
                    yAvavie=yAvavie+vitesseAvavieY;
                } 

                else if ((yAvavie>=0)){                         //Debut du timer
                    timervitesse1=timervitesse1+1;
                }

                if ((tentative>=2) && (yAvavie>=-130) && (timervitesse1>=100)){        
                    yAvavie=yAvavie-vitesseAvavieY;
                }

            sprite(xAvavie2, yAvavie2,"Asset/avavie2.bmp");

                if(((yAvavie2>=-130) && (yAvavie2<=0)) && (tentative==3) && (timervitesse5<50)){
                    yAvavie2=yAvavie2+vitesseAvavieY;
                } 

                else if ((yAvavie2>=0)){                         
                    timervitesse5=timervitesse5+1;
                }

                if ((tentative>=3) && (yAvavie2>=-130) && (timervitesse5>=100)){        
                    yAvavie2=yAvavie2-vitesseAvavieY;
                }

            sprite(xAvavitesse, yAvavitesse,"Asset/avavitesse1.bmp");

                if(((yAvavitesse>=-130) && (yAvavitesse<=0)) && ((vitesseX==4) || (vitesseX==-4)) && (timervitesse2<50)){
                    yAvavitesse=yAvavitesse+vitesseAvavieY;
                } 

                else if ((yAvavitesse>=0)){
                    timervitesse2=timervitesse2+1;
                }

                if ((yAvavitesse>=-130) && (timervitesse2>=100)){
                    yAvavitesse=yAvavitesse-vitesseAvavieY;
                }

            sprite(xAvavitesse2, yAvavitesse2,"Asset/avavitesse2.bmp");

                if(((yAvavitesse2>=-130) && (yAvavitesse2<=0)) && ((vitesseX==5) || (vitesseX==-5)) && (timervitesse3<50)){
                    yAvavitesse2=yAvavitesse2+vitesseAvavieY;
                } 

                else if ((yAvavitesse2>=0)){
                    timervitesse3=timervitesse3+1;
                }

                if ((yAvavitesse2>=-130) && (timervitesse3>=100)){
                    yAvavitesse2=yAvavitesse2-vitesseAvavieY;
                }

            sprite(xAvavitesse3, yAvavitesse3,"Asset/avavitesse3.bmp");

                if(((yAvavitesse3>=-130) && (yAvavitesse3<=0)) && ((vitesseX==6) || (vitesseX==-6)) && (timervitesse4<50)){
                    yAvavitesse3=yAvavitesse3+vitesseAvavieY;
                } 

                else if ((yAvavitesse3>=0)){
                    timervitesse4=timervitesse4+1;
                }

                if ((yAvavitesse3>=-130) && (timervitesse4>=100)){
                    yAvavitesse3=yAvavitesse3-vitesseAvavieY;
                }
}



/*-----------------------------------------Pressage de touche-------------------------------*/

void KeyPressed(SDL_Keycode touche){
    /** @brief event.key.keysym.sym renvoi la touche appuyé
     *
     */
    if(touche == SDLK_q) {
        printf("toucheQ %d \n",toucheQ);
        toucheQ=1;
    }

    if(touche == SDLK_d) {
        printf("toucheD %d \n",toucheD);
        toucheD=1;
    }
    if(touche == SDLK_LEFT) {
        printf("toucheLEFT %d \n",toucheLEFT);
        toucheLEFT=1;
    }

    if(touche == SDLK_RIGHT) {
        printf("toucheRIGHT %d \n",toucheRIGHT);
        toucheRIGHT=1;
    }
        // Voir doc SDL_Keycode pour plus de touches https://wiki.libsdl.org/SDL_Keycode
        // ici exemple ou appuyer sur les touches Q | D modifie x (position du carré dans drawGame())

//Autre


    if(touche == SDLK_ESCAPE){              //touche ESC appuyé //Ferme la fenetre arrete le jeu
            freeAndTerminate();
    }


    if(touche == SDLK_v){                   //touche v appuyé //Restart du jeu
            printf("V\n");
            init_game();
            init_tab();
    }


    if(touche == SDLK_p){                  //touche p appuyé //Met pause au jeu
            printf("p\n");
            PauseGame=PauseGame*(-1);
    }

//Deplacement Menu

    if(touche == SDLK_SPACE){                //touche Espace appuyé //Lance le jeu
            if ((start==0) && (solo==0) && (coop==0) && (versus==0)){
                printf("space\n");
                start=start+1;
                printf("start %d\n",start); 
                }

            else if ((start==1) && (solo==1) && (versus==0) && (coop==0)){
                printf("space\n");
                solo=solo+1;
                printf("solo %d\n",solo);
            }

            else if((start==1) && (solo==0) && (versus==0)  && (coop==1)){
                printf("space\n");
                coop=coop+1;
                printf("coop %d\n",coop);
            }

            else if ((start==1) && (coop==0) && (solo==0) && (versus==1)){
                printf("space\n");
                versus=versus+1;
                printf("versus %d\n",versus);
            }

    }

    if(touche == SDLK_a){                //touche Espace appuyé //Lance le jeu

            if ((start==1) && (solo==0) && (versus==0) && (coop==0)){
                printf("a\n");
                solo=solo+1;
                printf("solo %d\n",solo);
            }
    }

    if(touche == SDLK_z){                //touche Espace appuyé //Lance le jeu

            if ((start==1) && (solo==0) && (versus==0) && (coop==0)){
                printf("a\n");
                versus=versus+1;
                printf("versus %d\n",versus);
            }
    }

    if(touche == SDLK_e){              //touche Espace appuyé //Lance le jeu

            if ((start==1) && (solo==0) && (versus==0) && (coop==0)){
                printf("e\n");
                coop=coop+1;
                printf("coop %d\n",coop);
            }
    }

    if(touche == SDLK_x){                  //touche x start -1
            printf("x\n");

            if (solo == 1){
                solo=0;
            }

            if (solo >= 2){
                init_tab();
                init_game();
                start=1;
                solo=1;
            }

            if (coop == 1){
                coop=0;
            }

            if (coop>=2){
                init_tab();
                init_game();
                start=1;
                coop=1;
            }

            if (versus == 1){
                versus=0;
            }

            if (versus>=2){
                init_tab();
                init_game();
                start=1;
                versus=1;
            }
    }

}

void KeyReleased(SDL_Keycode touche){
    /** @brief event.key.keysym.sym renvoi la touche appuyé*/

    if(touche == SDLK_q) {
        printf("here\n");
        toucheQ=0;
    }

    if(touche == SDLK_d) {
        printf("here\n");
        toucheD=0;
    }
    if(touche == SDLK_LEFT) {
        printf("here\n");
        toucheLEFT=0;
    }

    if(touche == SDLK_RIGHT) {
        printf("here\n");
        toucheRIGHT=0;
    }
}

void BarreDeplacement(){

   if(toucheQ == 1) {
        if ((xBarre > largFenetre) && (PauseGame==1)){                       
            xBarre=xBarre-deplTouche;
        }
        else if (xBarre <= largFenetre){
            xBarre=largFenetre;
        }
        else;
    }

    if(toucheD == 1) {
        if ((xBarre < WINDOW_WIDTH-longBarre-largFenetre) && (PauseGame==1)){      
            xBarre=xBarre+deplTouche;
        }
        else if((xBarre >= WINDOW_WIDTH-longBarre-largFenetre) && (PauseGame==1)){
            xBarre = WINDOW_WIDTH-longBarre-largFenetre;
        }
        else;
    }
    if(toucheLEFT == 1) {
        if ((xBarre2 > largFenetre) && (PauseGame==1)){                       
            xBarre2=xBarre2-deplTouche;}
        else if (xBarre2 <= largFenetre){
            xBarre2=largFenetre;
        }
        else;
    }

    if(toucheRIGHT == 1) {
        if ((xBarre2 < WINDOW_WIDTH-longBarre2-largFenetre) && (PauseGame==1)){      
            xBarre2=xBarre2+deplTouche;
        }
        if((xBarre2 >= WINDOW_WIDTH-longBarre2-largFenetre) && (PauseGame==1)){
                xBarre2 = (WINDOW_WIDTH-largFenetre)-longBarre2;
        }
        else;
    }
}

void joyButtonPressed(){
}


/*/////////////////////////////////////////Mode Jeux\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
/*/////////////////////////////////////////Mode Jeux\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

void drawSolo(){
            
        if((PauseGame==1)){       //Jeux
            BarreDeplacement();            
            drawBackground();
            drawScore();
            moveBricky();
            moveBrickx();
            drawBrickMulti();  
            drawBalltraine2();
            drawBalltraine();
            drawBall();
            drawAccelarationMulti();
            drawBarre();
            rebondBall();
            rebondBarre ();
            drawPalier();
            drawCompteur();
            reninitpalier();
        }

        if((tentative>nbvie)){     //Defaite
            vitesseX=0;
            vitesseY=0;
            drawFinarcade();
            printf("Vous avez atteint le palier%d\n",accelPalier+1);
        }

        if((tentative>nbvie) && (solo>=3)){
            drawEnding();
        }
}

void drawCoop(){
            
        if((PauseGame==1) ){       //Jeux
            BarreDeplacement();           
            drawBackground();
            drawScore();
            drawCompteur();
            drawPalier();
            moveBrickmultiy();
            moveBrickx();
            drawBrickMulti();  
            drawBalltraine2();
            drawBalltraine();
            drawBall();
            drawAccelarationMulti();
            drawBarre();
            drawBarre2();
            rebondBall();
            rebondBarre ();
            rebondBarre2 ();
            reninitpalier();
        }

        if((tentative>nbvie)){     //Defaite
            vitesseX=0;
            vitesseY=0;
            drawFinarcade();
            printf("Vous avez atteint le palier%d\n",accelPalier+1);
        }

        if((tentative>nbvie) && (coop>=3)){
            drawEnding();
        }

}

void drawVersus(){
            
        if((PauseGame==1) ){       //Jeux
            BarreDeplacement();
            drawBackground();
            drawScoreVersus();
            drawCompteur();
            drawCompteurVie();
            drawPalier();
            moveBrickmultiy();
            moveBrickx();
            drawBrickMulti();  
            drawBalltraine2();
            drawBalltraine();
            drawBall();
            drawAccelarationMulti();
            drawBarre();
            drawBarreVersus();
            rebondBall();
            rebondBarre ();
            rebondBarre2 ();
            reninitpalier();
        }

        if((tentative>nbvie)){     //Defaite
            vitesseX=0;
            vitesseY=0;
            drawFinversus();
            printf("Vous avez atteint le palier%d\n",accelPalier+1);
        }

        else if((tentativeJ2>nbvieJ2)){     //Defaite
            vitesseX=0;
            vitesseY=0;
            drawFinversus();
            printf("Vous avez atteint le palier%d\n",accelPalier+1);
        }

        if(((tentative>nbvie)||(tentativeJ2>nbvieJ2)) && (versus>=3)){
            drawEnding();
        }

}


/*/////////////////////////////////////////Jeu\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


void drawGame(){

    clear();

        drawAll();

        if((start==0) && (coop==0) && (solo==0) && (versus==0) ){                                 //Ecran d'accueil
            drawAccueil();
        }

        if((start==1) && (coop==0) && (solo==0) && (versus==0) ){                            //Ecran de menu
            drawMenu();
        }
        
        if ((start==1) && (coop==0) && (solo==1) && (versus==0)){ //Explication solo
            drawExplisolo();
        }

        if ((start==1) && (versus==0) && (coop==0) && (solo>=2)){ //lance le jeu solo
            drawSolo();
        }

        if((start==1) && (solo==0) && (versus==1) && (coop==0)){ //Explication versus
            drawExpliVersus();
        }

        if((start==1) && (solo==0) && (versus>=2) && (coop==0)){ //Explication versus
            drawVersus();
        }

        if((start==1) && (solo==0) && (coop==1) && (versus==0)){ //Explication versus
            drawExpliCoop();
        }

        if((start==1) && (solo==0) && (coop>=2) && (versus==0)){ //Explication versus
            drawCoop();
        }

        if (PauseGame==1){                                                              // Si jeu != en pause
            actualize();
        }

    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
}


/*-----------------------------------------GameLoop-----------------------------------------*/

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        // Boucle pour garder le programme ouvert
        // lorsque programLaunched est different de 1
        // on sort de la boucle, donc de la fonction
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            // boucle pour la gestion d'évenement
            switch (event.type) {
                /** event.type renvoi le type d'evenement qui se passe
                 * (si on appuie sur une touche, clique de souris...)
                 * en fonction du type d'évènement on à alors
                 * différentes information accessibles
                 * voir doc pour plus d'event https://wiki.libsdl.org/SDL_EventType
                 */
                case SDL_QUIT:
                    // quand on clique sur fermer la fénêtre en haut à droite
                    programLaunched = 0;
                    break;

                case SDL_MOUSEBUTTONUP:
                    /* clique de la souris
                     * event.motion.y | event.motion.x pour les positions de la souris
                     */
                    printf("position de la souris x : %d , y : %d\n", (event.motion.x), event.motion.y);
                    break;

                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;

                case SDL_KEYUP:
                    KeyReleased(event.key.keysym.sym);
                    break;

                case SDL_JOYBUTTONDOWN:
                    break;
                default:
                    break;
            }
        }
        /* coder ici pour que le code s'execute après chaque évenement
         * exemple dessiner un carré avec position int x, int y qu'on peut
         * deplacer lorsqu'on appuie sur une touche
         */
        drawGame();
    }
}

int main(){
    /** @description 3 fonctions dans le main qui permettent de créer l'application et la maintenir ouverte :
     *  init(...) : initialiser la SDL/ fenêtre
     *  gameLoop() : boucle de jeu dans laquelle l'application reste ouverte
     *  freeAndTerminate() : quitte le programme proprement
     */
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    init_tab();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}